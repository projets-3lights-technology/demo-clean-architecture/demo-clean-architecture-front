import {PokemonHandler} from '../src/app/pokemon/usecases/pokemon.handler'
import {PokemonLoader} from '../src/app/pokemon/domain/loaders/PokemonLoader'
import {InMemoryPokemonLoader} from '../src/app/pokemon/adapters/secondaries/inmemory/inMemoryPokemon.loader'
import {Pokemon} from '../src/app/pokemon/domain/entities/pokemon'
import {StubPokemonBuilder} from './stubs/stubPokemon.builder'

describe('Pokemon handler fetches', () => {

    let pikachu: Pokemon

    beforeEach(() => {
        pikachu = new StubPokemonBuilder().withName('pikachu').build()
    })

    describe('A list', () => {

        it('With zero pokemon if there is no pokemon in the source', done => {
            const pokemonHandler = createPokemonHandler([])

            pokemonHandler.all().subscribe(pokemons => {
                expect(pokemons).toEqual([])
                done()
            })
        })

        it('With one pokemon if there is one pokemon in the source', done => {
            const pokemonHandler = createPokemonHandler([pikachu])

            pokemonHandler.all().subscribe(pokemons => {
                verifyListOfPokemons(pokemons, [pikachu])
                done()
            })
        })


        it('With two pokemons if there are two pokemons in the source', done => {
            const salameche: Pokemon = new StubPokemonBuilder().withNumber('2').withName('salameche').build()
            const pokemonHandler = createPokemonHandler([pikachu, salameche])

            pokemonHandler.all().subscribe(pokemons => {
                verifyListOfPokemons(pokemons, [pikachu, salameche])
                done()
            })
        })

        function verifyListOfPokemons(pokemons: Pokemon[], expectedPokemons: Pokemon[]) {
            expect(pokemons.length).toEqual(expectedPokemons.length)
            expectedPokemons.forEach((expectedPokemon, index) => verifyOnePokemon(pokemons[index], expectedPokemon))
        }

    })


    it('A details of one pokemon', done => {
        const salameche: Pokemon = new StubPokemonBuilder().withNumber('2').withName('salameche').build()
        const pokemonHandler = createPokemonHandler([pikachu, salameche])

        pokemonHandler.get('2').subscribe(pokemon => {
            verifyOnePokemon(pokemon, salameche)
            done()
        })
    })

    function createPokemonHandler(pokemonPopulation: Pokemon[]) {
        const pokemonSource: PokemonLoader = new InMemoryPokemonLoader(pokemonPopulation)
        return new PokemonHandler(pokemonSource)
    }

    function verifyOnePokemon(pokemon: Pokemon, expectedPokemon: Pokemon) {
        expect(pokemon.number).toEqual(expectedPokemon.number)
        expect(pokemon.name).toEqual(expectedPokemon.name)
        expect(pokemon.description).toEqual(expectedPokemon.description)
        expect(pokemon.weight).toEqual(expectedPokemon.weight)
        expect(pokemon.height).toEqual(expectedPokemon.height)
        expect(pokemon.avatar).toEqual(expectedPokemon.avatar)
    }

})