import {PokemonDTO} from '../../src/app/pokemon/adapters/secondaries/real/DTO/PokemonDTO'
import {Pokemon} from '../../src/app/pokemon/domain/entities/pokemon'
import {PokemonBuilder} from '../../src/app/pokemon/usecases/pokemon.builder'
import {of} from 'rxjs/internal/observable/of'
import {PokemonHandler} from '../../src/app/pokemon/usecases/pokemon.handler'
import {PokemonLoader} from '../../src/app/pokemon/domain/loaders/PokemonLoader'
import {FirebasePokemonLoader} from '../../src/app/pokemon/adapters/secondaries/real/firebasePokemon.loader'
import {FirebaseClient} from '../../src/app/pokemon/adapters/secondaries/real/firebase/FirebaseClient'

describe('Integration | Firebase pokemon loader fetches', () => {

    let fireBaseSpy: FirebaseClient
    let fakePokemonResponse: PokemonDTO
    let expectedPokemon: Pokemon

    let pokemonLoader: PokemonLoader
    let pokemonHandler: PokemonHandler

    beforeEach(() => {
        fireBaseSpy = {list: null, object: null} as FirebaseClient
        fakePokemonResponse = {
            id: 'id',
            name: 'name',
            description: 'lorem ipsum',
            height: 2,
            weight: 3,
            picture_url: 'url'
        }
        expectedPokemon = new PokemonBuilder()
            .withNumber('id')
            .withName('name')
            .withDescription('lorem ipsum')
            .withHeight(2)
            .withWeight(3)
            .withAvatar('url')
            .build()
        pokemonLoader = new FirebasePokemonLoader(fireBaseSpy)
        pokemonHandler = new PokemonHandler(pokemonLoader)
    })

    it('A list of pokemons', done => {
        spyOn(fireBaseSpy, 'list').and.returnValue(of([fakePokemonResponse]))

        pokemonHandler.all().subscribe(pokemons => {
            verifyPokemonList(pokemons, [expectedPokemon])
            expect(fireBaseSpy.list).toHaveBeenCalledTimes(1)
            expect(fireBaseSpy.list).toHaveBeenCalledWith('/pokemons')
            done()
        })
    })

    it('A details of one pokemon', done => {
        spyOn(fireBaseSpy, 'object').and.returnValue(of(fakePokemonResponse))

        pokemonHandler.get('1').subscribe(pokemon => {
            verifyOnePokemon(pokemon, expectedPokemon)
            expect(fireBaseSpy.object).toHaveBeenCalledTimes(1)
            expect(fireBaseSpy.object).toHaveBeenCalledWith('/pokemons', '1')
            done()
        })
    })

    function verifyPokemonList(pokemons: Pokemon[], expectedPokemons: Pokemon[]) {
        expect(pokemons.length).toEqual(expectedPokemons.length)
        expectedPokemons.forEach((expectedPokemon, index) => verifyOnePokemon(pokemons[index], expectedPokemon))
    }

    function verifyOnePokemon(pokemon: Pokemon, expectedPokemon: Pokemon) {
        expect(pokemon.number).toEqual(expectedPokemon.number)
        expect(pokemon.name).toEqual(expectedPokemon.name)
        expect(pokemon.description).toEqual(expectedPokemon.description)
        expect(pokemon.weight).toEqual(expectedPokemon.weight)
        expect(pokemon.height).toEqual(expectedPokemon.height)
        expect(pokemon.avatar).toEqual(expectedPokemon.avatar)
    }

})