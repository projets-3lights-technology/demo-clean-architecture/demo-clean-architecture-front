import {PokemonHandler} from '../../src/app/pokemon/usecases/pokemon.handler'
import {Pokemon} from '../../src/app/pokemon/domain/entities/pokemon'
import {PokemonLoader} from '../../src/app/pokemon/domain/loaders/PokemonLoader'
import {PokemonDTO} from '../../src/app/pokemon/adapters/secondaries/real/DTO/PokemonDTO'
import {PokemonBuilder} from '../../src/app/pokemon/usecases/pokemon.builder'
import {GraphQLPokemonLoader} from '../../src/app/pokemon/adapters/secondaries/real/graphQLPokemon.loader'
import {GraphQLClient} from '../../src/app/pokemon/adapters/secondaries/real/graphQL/GraphQLClient'

describe('Integration | GraphQL pokemon loader', () => {

    let apolloClientSpy: GraphQLClient
    let fakePokemonResponse: PokemonDTO
    let expectedPokemon: Pokemon

    let pokemonLoader: PokemonLoader
    let pokemonHandler: PokemonHandler

    beforeEach(() => {
        apolloClientSpy = {query: null} as GraphQLClient
        fakePokemonResponse = {
            id: 'id',
            name: 'name',
            description: 'lorem ipsum',
            height: 2,
            weight: 3,
            picture_url: 'url'
        }
        expectedPokemon = new PokemonBuilder()
            .withNumber('id')
            .withName('name')
            .withDescription('lorem ipsum')
            .withHeight(2)
            .withWeight(3)
            .withAvatar('url')
            .build()
        pokemonLoader = new GraphQLPokemonLoader(apolloClientSpy)
        pokemonHandler = new PokemonHandler(pokemonLoader)
    })

    it('A list of pokemons', done => {
        const query = `
            {
                pokemons {
                    id
                    name
                    description
                    height
                    weight
                    picture_url
                }
            }
        `
        spyOn(apolloClientSpy, 'query').and.returnValue(Promise.resolve({
            pokemons: [fakePokemonResponse]
        }))

        pokemonHandler.all().subscribe(pokemons => {
            verifyPokemonList(pokemons, [expectedPokemon])
            expect(apolloClientSpy.query).toHaveBeenCalledTimes(1)
            expect(apolloClientSpy.query).toHaveBeenCalledWith({query})
            done()
        })
    })

    it('A details of one pokemon', done => {
        const query = `query($id: String!)
            {
                pokemon(id: $id) {
                    id
                    name
                    description
                    height
                    weight
                    picture_url
                }
            }
        `
        spyOn(apolloClientSpy, 'query').and.returnValue(Promise.resolve({
            pokemon: fakePokemonResponse
        }))

        pokemonHandler.get('1').subscribe(pokemon => {
            verifyOnePokemon(pokemon, expectedPokemon)
            expect(apolloClientSpy.query).toHaveBeenCalledTimes(1)
            expect(apolloClientSpy.query).toHaveBeenCalledWith({query, variables: {id: '1'}})
            done()
        })
    })

    function verifyPokemonList(pokemons: Pokemon[], expectedPokemons: Pokemon[]) {
        expect(pokemons.length).toEqual(expectedPokemons.length)
        expectedPokemons.forEach((expectedPokemon, index) => verifyOnePokemon(pokemons[index], expectedPokemon))
    }

    function verifyOnePokemon(pokemon: Pokemon, expectedPokemon: Pokemon) {
        expect(pokemon.number).toEqual(expectedPokemon.number)
        expect(pokemon.name).toEqual(expectedPokemon.name)
        expect(pokemon.description).toEqual(expectedPokemon.description)
        expect(pokemon.weight).toEqual(expectedPokemon.weight)
        expect(pokemon.height).toEqual(expectedPokemon.height)
        expect(pokemon.avatar).toEqual(expectedPokemon.avatar)
    }

})