import {HttpClient} from '@angular/common/http'
import {of} from 'rxjs/internal/observable/of'
import {PokemonLoader} from '../../src/app/pokemon/domain/loaders/PokemonLoader'
import {PokemonDTO} from '../../src/app/pokemon/adapters/secondaries/real/DTO/PokemonDTO'
import {Pokemon} from '../../src/app/pokemon/domain/entities/pokemon'
import {PokemonHandler} from '../../src/app/pokemon/usecases/pokemon.handler'
import {PokemonBuilder} from '../../src/app/pokemon/usecases/pokemon.builder'
import {RESTPokemonLoader} from '../../src/app/pokemon/adapters/secondaries/real/RESTPokemon.loader'

describe('Integration | REST Pokemon loader fetches', () => {

    let httpFake: HttpClient
    let pokemonLoader: PokemonLoader
    let pokemonHandler: PokemonHandler
    let fakePokemonResponse: PokemonDTO
    let pokemon: Pokemon

    beforeAll(() => {
        httpFake = {get: null} as HttpClient
    })

    beforeEach(() => {
        pokemonLoader = new RESTPokemonLoader(httpFake)
        pokemonHandler = new PokemonHandler(pokemonLoader)
        fakePokemonResponse = {
            id: '1',
            name: 'name',
            weight: 2,
            height: 4,
            picture_url: 'picture',
            description: 'Lorem ipsum'
        }
        pokemon = new PokemonBuilder()
            .withNumber('1')
            .withName('name')
            .withWeight(2)
            .withHeight(4)
            .withAvatar('picture')
            .withDescription('Lorem ipsum')
            .build()
    })

    it('A list of pokemons', done => {
        spyOn(httpFake, 'get').and.returnValue(of([fakePokemonResponse]))

        pokemonHandler.all().subscribe(pokemons => {
            verifyPokemonList(pokemons, [pokemon])
            expect(httpFake.get).toHaveBeenCalled()
            expect(httpFake.get).toHaveBeenCalledWith('http://demo-pokemon-backend.herokuapp.com/api/v1/pokemons')
            done()
        })
    })

    it('A details of one pokemon', done => {
        spyOn(httpFake, 'get').and.returnValue(of(fakePokemonResponse))

        pokemonHandler.get('1').subscribe(pokemon => {
            verifyOnePokemon(pokemon, pokemon)
            expect(httpFake.get).toHaveBeenCalled()
            expect(httpFake.get).toHaveBeenCalledWith('http://demo-pokemon-backend.herokuapp.com/api/v1/pokemons/1')
            done()
        })
    })

    function verifyPokemonList(pokemons: Pokemon[], expectedPokemons: Pokemon[]) {
        expect(pokemons.length).toEqual(expectedPokemons.length)
        expectedPokemons.forEach((expectedPokemon, index) => verifyOnePokemon(pokemons[index], expectedPokemon))
    }

    function verifyOnePokemon(pokemon: Pokemon, expectedPokemon: Pokemon) {
        expect(pokemon.number).toEqual(expectedPokemon.number)
        expect(pokemon.name).toEqual(expectedPokemon.name)
        expect(pokemon.description).toEqual(expectedPokemon.description)
        expect(pokemon.weight).toEqual(expectedPokemon.weight)
        expect(pokemon.height).toEqual(expectedPokemon.height)
        expect(pokemon.avatar).toEqual(expectedPokemon.avatar)
    }

})
