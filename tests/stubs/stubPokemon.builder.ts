import {PokemonBuilder} from '../../src/app/pokemon/usecases/pokemon.builder'

export class StubPokemonBuilder extends PokemonBuilder {

    protected _number: String = "1"
    protected _name: String = "pikachu"
    protected _description: String = "Lorem ipsum of pikachu"
    protected _weight: number = 0.6
    protected _height: number = 1
    protected _avatar: String = "avatar"

}