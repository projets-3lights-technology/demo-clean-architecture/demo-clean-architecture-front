# Demo Clean Architecture Front

[![pipeline status](https://gitlab.com/mickaelw/demo-clean-architecture-frontend/badges/master/pipeline.svg)](https://gitlab.com/mickaelw/demo-clean-architecture-frontend/commits/master)

### Installation

```
yarn or npm i
```

### Launch test

```
yarn test or npm test
```

### Launch server
```
- yarn start or npm start
- yarn start:inmemory or npm start:inmemory
```
