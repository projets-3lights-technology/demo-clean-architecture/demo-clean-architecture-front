const webpackMerge = require('webpack-merge')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const commonConfig = require('./webpack.common.js')
const helpers = require('./helpers')

const Dotenv = require('dotenv-webpack')

module.exports = webpackMerge(commonConfig, {
    mode: 'development',

    devtool: 'cheap-module-eval-source-map',

    output: {
        path: helpers.root('dist'),
        publicPath: '/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    plugins: [
        new ExtractTextPlugin('[name].css'),
        new Dotenv({
            path: helpers.root('.env'),
            systemvars: true,
        })
    ],

    devServer: {
        port: 8080,
        inline: true,
        historyApiFallback: true,
        stats: 'minimal'
    }
})