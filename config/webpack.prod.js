const webpack = require('webpack')
const webpackMerge = require('webpack-merge')
const ExtractCssChunks = require("extract-css-chunks-webpack-plugin")
const commonConfig = require('./webpack.common.js')
const helpers = require('./helpers')

const Dotenv = require('dotenv-webpack')

module.exports = webpackMerge(commonConfig, {
    mode: 'production',

    devtool: 'source-map',

    output: {
        path: helpers.root('dist'),
        publicPath: '/',
        filename: '[name].[hash].js',
        chunkFilename: '[id].[hash].chunk.js'
    },

    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.UglifyJsPlugin({
            mangle: {
                keep_fnames: true
            }
        }),
        new ExtractCssChunks({filename: '[name].[hash].css'}),
        new webpack.DefinePlugin({
            'process.env': {
                'ENV': JSON.stringify(ENV)
            }
        }),
        new webpack.LoaderOptionsPlugin({
            htmlLoader: {
                minimize: false
            }
        }),
        new Dotenv({
            path: helpers.root('.env'),
            systemvars: true,
        })
    ]
})