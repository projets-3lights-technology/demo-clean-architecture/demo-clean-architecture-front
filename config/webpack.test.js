const webpack = require('webpack')
const helpers = require('./helpers')
const Dotenv = require('dotenv-webpack')

module.exports = {
    devtool: 'inline-source-map',

    resolve: {
        extensions: ['.ts', '.js']
    },

    module: {
        rules: [
            { test: /\.ts$/, loaders: ['awesome-typescript-loader', 'angular2-template-loader'] },
            { test: /\.html$/, loader: 'html-loader' },
            { test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=\.]+)?$/, loader: 'file-loader?name=fonts/[name].[ext]' },
            { test: /\.(jpe?g|png|gif)$/i, loader: "file-loader?name=images/[name].[ext]" },
            { test: /\.(css|scss)$/, loaders: ['to-string-loader', 'css-loader', 'sass-loader'] }
        ]
    },

    plugins: [
        new webpack.ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)@angular/,
            helpers.root('./src'),
            {}
        ),
        new Dotenv({
            path: helpers.root('.env.test'),
            systemvars: true,
        })
    ]
}