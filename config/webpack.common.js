const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const helpers = require('./helpers')

module.exports = {
    entry: {
        'polyfills': './src/polyfills.ts',
        'vendor': './src/vendor.ts',
        'app': './src/main.ts'
    },

    resolve: {
        extensions: ['.ts', '.js']
    },

    module: {
        rules: [
            { test: /\.ts$/, loaders: ['awesome-typescript-loader', 'angular2-template-loader'] },
            { test: /\.html$/, loader: 'html-loader' },
            { test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=\.]+)?$/, loader: 'file-loader?name=fonts/[name].[ext]' },
            { test: /\.(jpe?g|png|gif)$/i, loader: "file-loader?name=images/[name].[ext]" },
            { test: /\.(css|scss)$/, loaders: ['to-string-loader', 'css-loader', 'sass-loader'] }
        ]
    },

    optimization: {
        splitChunks: {
            chunks: "all"
        }
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        })
    ]
}