const webpackConfig = require('./webpack.test')

module.exports = function (config) {
    const _config = {
        basePath: '../',

        frameworks: ['jasmine'],

        files: [
            {pattern: './config/karma-test-shim.js', watched: false}
        ],

        plugins: [
            require('karma-jasmine'),
            require('karma-webpack'),
            require('karma-sourcemap-loader'),
            require('karma-jasmine-html-reporter'),
            require('karma-phantomjs-launcher'),
        ],

        preprocessors: {
            './config/karma-test-shim.js': ['webpack', 'sourcemap']
        },

        webpack: webpackConfig,

        webpackMiddleware: {
            stats: 'errors-only'
        },

        webpackServer: {
            noInfo: true
        },

        reporters: ['progress', 'kjhtml'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: false,
        browsers: ['PhantomJS'],
        singleRun: true
    }

    config.set(_config)
}