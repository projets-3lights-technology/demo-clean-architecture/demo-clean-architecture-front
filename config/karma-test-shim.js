Error.stackTraceLimit = Infinity;

require('core-js/es6');
require('core-js/es7/reflect');

require('zone.js/dist/zone');
require('zone.js/dist/zone-testing');

const appContext = require.context('../tests', true, /\.spec\.ts/)

appContext.keys().forEach(appContext);

const testing = require('@angular/core/testing')
const browser = require('@angular/platform-browser-dynamic/testing')

testing.TestBed.initTestEnvironment(browser.BrowserDynamicTestingModule, browser.platformBrowserDynamicTesting());