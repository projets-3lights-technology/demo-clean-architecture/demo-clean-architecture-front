import {Component, Input} from '@angular/core'
import {Pokemon} from "../../../../domain/entities/pokemon"

@Component({
    selector: 'pokemon-listing-item',
    templateUrl: './pokemonListingItem.component.html',
    styleUrls: ['./pokemonListingItem.component.scss']
})
export class PokemonListingItemComponent {

    @Input() pokemon: Pokemon

}