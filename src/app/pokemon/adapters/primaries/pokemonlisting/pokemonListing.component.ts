import {Component, Inject, OnInit} from '@angular/core'
import {PokemonHandler} from '../../../usecases/pokemon.handler'
import {Observable} from 'rxjs/Observable'
import {Pokemon} from '../../../domain/entities/pokemon'
import {PokemonDIProvider} from '../../../configuration/pokemonDI.provider'

@Component({
    templateUrl: './pokemonListing.component.html',
    styleUrls: ['./pokemonListing.component.scss']
})
export class PokemonListingComponent implements OnInit {

    pokemonList$: Observable<Pokemon[]>

    constructor(@Inject(PokemonDIProvider.pokemonHandler) private pokemonHandler: PokemonHandler) {
    }

    ngOnInit(): void {
        this.pokemonList$ = this.pokemonHandler.all()
    }

}