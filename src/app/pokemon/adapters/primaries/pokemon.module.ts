import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap'

import {FormsModule} from '@angular/forms'
import {PokemonListingComponent} from './pokemonlisting/pokemonListing.component'
import {PokemonDetailsComponent} from './pokemondetails/pokemonDetails.component'
import {RouterModule} from '@angular/router'
import {PokemonRoutes} from '../../configuration/pokemon.routes'
import {PokemonListingItemComponent} from './pokemonlisting/pokemonlistingitem/pokemonListingItem.component'

import {HttpClient, HttpClientModule} from '@angular/common/http'
import {PokemonHandler} from '../../usecases/pokemon.handler'
import {AngularFireDatabaseModule} from 'angularfire2/database'
import {ApolloGraphQLClient} from '../secondaries/real/graphQL/apollo.graphQLClient'
import {GraphQLClient} from '../secondaries/real/graphQL/GraphQLClient'
import {StandardFirebaseClient} from '../secondaries/real/firebase/standardFirebase.client'
import {FirebaseClient} from '../secondaries/real/firebase/FirebaseClient'
import {PokemonDIFactory} from '../../configuration/pokemonDI.factory'
import {PokemonDIProvider} from '../../configuration/pokemonDI.provider'

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        NgbModule,
        RouterModule.forChild(PokemonRoutes),
        HttpClientModule,
        AngularFireDatabaseModule
    ],
    declarations: [
        PokemonListingComponent,
        PokemonListingItemComponent,
        PokemonDetailsComponent
    ],
    exports: [
        PokemonListingComponent,
        PokemonDetailsComponent
    ],
    providers: [
        StandardFirebaseClient,
        ApolloGraphQLClient,
        {
            provide: PokemonDIProvider.pokemonHandler,
            useFactory: (http: HttpClient, firebase: FirebaseClient, graphQLClient: GraphQLClient) =>
                new PokemonHandler(PokemonDIFactory.pokemonLoader(http, firebase, graphQLClient))
            ,
            deps: [HttpClient, StandardFirebaseClient, ApolloGraphQLClient]
        }
    ]
})
export class PokemonModule {
}