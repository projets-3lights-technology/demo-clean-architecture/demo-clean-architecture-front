import {Component, Inject, OnInit} from '@angular/core'
import {Observable} from 'rxjs/Observable'
import {Pokemon} from '../../../domain/entities/pokemon'
import {PokemonHandler} from '../../../usecases/pokemon.handler'
import {ActivatedRoute} from '@angular/router'
import {PokemonDIProvider} from '../../../configuration/pokemonDI.provider'

@Component({
    templateUrl: './pokemonDetails.component.html',
    styleUrls: ['./pokemonDetails.component.scss']
})
export class PokemonDetailsComponent implements OnInit {

    pokemon$: Observable<Pokemon>

    constructor(@Inject(PokemonDIProvider.pokemonHandler) private pokemonHandler: PokemonHandler,
                private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.pokemon$ = this.pokemonHandler.get(this.route.snapshot.paramMap.get('number'))
    }

}