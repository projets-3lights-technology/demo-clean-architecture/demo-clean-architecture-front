export interface PokemonDTO {
    id: String
    name: String
    picture_url: String
    height: number
    weight: number,
    description: String
}