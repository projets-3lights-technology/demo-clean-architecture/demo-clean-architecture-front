export interface QueryRequestParams {
    query: String,
    variables?: { [key: string]: String }
}

export interface GraphQLClient {

    query<D>(params: QueryRequestParams): Promise<D>

}