import {GraphQLClient, QueryRequestParams} from './GraphQLClient'
import {ApolloFetch, createApolloFetch, GraphQLRequest} from 'apollo-fetch'

export class ApolloGraphQLClient implements GraphQLClient {

    private apolloClient: ApolloFetch

    constructor() {
        this.apolloClient = createApolloFetch({uri: process.env.GQL_ENDPOINT})
    }

    query<D>(params: QueryRequestParams): Promise<D> {
        return this.apolloClient(params as GraphQLRequest).then(res => res.data)
    }

}