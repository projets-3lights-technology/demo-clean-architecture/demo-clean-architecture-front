import {PokemonLoader} from '../../../domain/loaders/PokemonLoader'
import {Pokemon} from '../../../domain/entities/pokemon'
import {Observable} from 'rxjs/Observable'
import {fromPromise} from 'rxjs/internal-compatibility'
import {map} from 'rxjs/operators'
import {PokemonDTO} from './DTO/PokemonDTO'
import {GraphQLClient} from './graphQL/GraphQLClient'
import {PokemonMapper} from './mappers/pokemon.mapper'

export class GraphQLPokemonLoader implements PokemonLoader {

    constructor(private apolloClient: GraphQLClient) {
    }

    all(): Observable<Pokemon[]> {
        const query = `
            {
                pokemons {
                    id
                    name
                    description
                    height
                    weight
                    picture_url
                }
            }
        `
        return fromPromise(this.apolloClient.query<{ pokemons: PokemonDTO[] }>({query}))
            .pipe(
                map(qraphQLResult => qraphQLResult.pokemons),
                map<PokemonDTO[], Pokemon[]>(pokemons => pokemons.map(PokemonMapper.mapToPokemon))
            )
    }

    get(number: String): Observable<Pokemon> {
        const query = `query($id: String!)
            {
                pokemon(id: $id) {
                    id
                    name
                    description
                    height
                    weight
                    picture_url
                }
            }
        `

        return fromPromise(this.apolloClient.query<{ pokemon: PokemonDTO }>({query, variables: {id: number}}))
            .pipe(
                map(qraphQLResult => qraphQLResult.pokemon),
                map<PokemonDTO, Pokemon>(PokemonMapper.mapToPokemon)
            )
    }

}