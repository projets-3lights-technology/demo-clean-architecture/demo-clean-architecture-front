import {Observable} from 'rxjs/Observable'

export interface FirebaseClient {

    list<D>(ref: String): Observable<D[]>

    object<D>(ref: String, id: String): Observable<D>

}