import { FirebaseClient } from './FirebaseClient'
import * as firebase from 'firebase'
import { Observable } from 'rxjs/Observable'
import { Subject } from 'rxjs/Subject'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

export class StandardFirebaseClient implements FirebaseClient {

    private app: firebase.app.App
    private db: firebase.database.Database
    private config = {
        apiKey: process.env.FB_API_KEY,
        authDomain: process.env.FB_AUTH_DOMAIN,
        databaseURL: process.env.FB_DB_URL,
        projectId: process.env.FB_PROJECT_ID,
        storageBucket: process.env.FB_STORAGE_BUCKET,
        messagingSenderId: process.env.FB_MESSAGING_SENDER_ID
    }

    private resultList$: Subject<any> = new BehaviorSubject([])
    private result$: Subject<any> = new BehaviorSubject(undefined)

    constructor() {
        this.app = firebase.apps.length === 0 ?
            firebase.initializeApp(this.config, process.env.FB_APP_NAME) : firebase.app()

        this.db = this.app.database()
    }

    list<D>(ref: String): Observable<D[]> {
        this.db.ref(ref.toString()).on('value', dataSnapshot => {
            const dataList = this.snapshotToArray<D>(dataSnapshot)
            this.resultList$.next(dataList)
        })
        return this.resultList$
    }

    object<D>(ref: String, id: String): Observable<D> {
        this.db.ref(ref.toString()).child(id.toString()).on('value', dataSnapshot => {
            const data = dataSnapshot.val() as D
            this.result$.next(data)
        })
        return this.result$
    }

    private snapshotToArray<D>(snapshot: firebase.database.DataSnapshot): D[] {
        let returnArr: D[] = []
        snapshot.forEach(item => {
            returnArr = returnArr.concat(item.val())
        })
        return returnArr
    }

}