import {Pokemon} from '../../../../domain/entities/pokemon'
import {PokemonDTO} from '../DTO/PokemonDTO'
import {PokemonBuilder} from '../../../../usecases/pokemon.builder'

export class PokemonMapper {
    static mapToPokemon(pokemon: PokemonDTO): Pokemon {
        return new PokemonBuilder()
            .withNumber(pokemon.id)
            .withName(pokemon.name)
            .withDescription(pokemon.description)
            .withHeight(pokemon.height)
            .withWeight(pokemon.weight)
            .withAvatar(pokemon.picture_url)
            .build()
    }
}