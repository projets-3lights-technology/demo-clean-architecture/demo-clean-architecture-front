import {PokemonLoader} from '../../../domain/loaders/PokemonLoader'
import {Pokemon} from '../../../domain/entities/pokemon'
import {Observable} from 'rxjs/Observable'
import {HttpClient} from '@angular/common/http'
import {map} from 'rxjs/operators'
import {PokemonDTO} from './DTO/PokemonDTO'
import {PokemonMapper} from './mappers/pokemon.mapper'

export class RESTPokemonLoader implements PokemonLoader {

    constructor(private http: HttpClient) {
    }

    all(): Observable<Pokemon[]> {
        return this.http
            .get(process.env.REST_ENDPOINT + '/pokemons')
            .pipe(
                map<PokemonDTO[], Pokemon[]>(pokemons => pokemons.map(PokemonMapper.mapToPokemon))
            )
    }

    get(number: String): Observable<Pokemon> {
        return this.http
            .get(process.env.REST_ENDPOINT + '/pokemons/' + number)
            .pipe(
                map<PokemonDTO, Pokemon>(PokemonMapper.mapToPokemon)
            )
    }
}