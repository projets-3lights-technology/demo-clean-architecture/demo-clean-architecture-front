import {PokemonLoader} from '../../../domain/loaders/PokemonLoader'
import {Pokemon} from '../../../domain/entities/pokemon'
import {Observable} from 'rxjs/Observable'
import {PokemonDTO} from './DTO/PokemonDTO'
import {FirebaseClient} from './firebase/FirebaseClient'
import {map} from 'rxjs/operators'
import {PokemonMapper} from './mappers/pokemon.mapper'


export class FirebasePokemonLoader implements PokemonLoader {

    constructor(private firebaseClient: FirebaseClient) {
    }

    all(): Observable<Pokemon[]> {
        return this.firebaseClient
            .list<PokemonDTO>('/pokemons')
            .pipe(
                map<PokemonDTO[], Pokemon[]>(pokemons => pokemons.map(PokemonMapper.mapToPokemon))
            )
    }

    get(number: String): Observable<Pokemon> {
        return this.firebaseClient
            .object<PokemonDTO>('/pokemons', number)
            .pipe(
                map<PokemonDTO, Pokemon>(PokemonMapper.mapToPokemon)
            )
    }

}