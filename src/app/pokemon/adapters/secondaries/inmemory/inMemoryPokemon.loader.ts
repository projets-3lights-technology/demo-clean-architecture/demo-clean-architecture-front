import {PokemonLoader} from '../../../domain/loaders/PokemonLoader'
import {Observable} from 'rxjs/Observable'
import {Pokemon} from '../../../domain/entities/pokemon'
import {Subject} from 'rxjs/Subject'
import {BehaviorSubject} from 'rxjs/BehaviorSubject'
import {map} from 'rxjs/operators'

export class InMemoryPokemonLoader implements PokemonLoader {

    private pokemons$: Subject<Pokemon[]> = new BehaviorSubject(this.pokemons)

    constructor(private pokemons: Pokemon[]) {

    }

    all(): Observable<Pokemon[]> {
        return this.pokemons$
    }

    get(number: String): Observable<Pokemon> {
        return this.pokemons$
            .pipe(
                map(pokemons => pokemons.filter(pokemon => pokemon.number === number)[0])
            )
    }

}