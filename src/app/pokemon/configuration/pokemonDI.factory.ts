import {PokemonLoader} from '../domain/loaders/PokemonLoader'
import {HttpClient} from '@angular/common/http'
import {RESTPokemonLoader} from '../adapters/secondaries/real/RESTPokemon.loader'
import {InMemoryPokemonLoader} from '../adapters/secondaries/inmemory/inMemoryPokemon.loader'
import {PokemonBuilder} from '../usecases/pokemon.builder'
import {Pokemon} from '../domain/entities/pokemon'
import {FirebaseClient} from '../adapters/secondaries/real/firebase/FirebaseClient'
import {GraphQLClient} from '../adapters/secondaries/real/graphQL/GraphQLClient'
import {FirebasePokemonLoader} from '../adapters/secondaries/real/firebasePokemon.loader'
import {GraphQLPokemonLoader} from '../adapters/secondaries/real/graphQLPokemon.loader'

export class PokemonDIFactory {

    static pokemonLoader(http: HttpClient, firebaseClient: FirebaseClient, graphQLClient: GraphQLClient): PokemonLoader {
        switch (process.env.MODE) {
            case 'real':
                switch (process.env.SOURCE) {
                    case 'graphql':
                        return new GraphQLPokemonLoader(graphQLClient)
                    case 'firebase':
                        return new FirebasePokemonLoader(firebaseClient)
                    default:
                        return new RESTPokemonLoader(http)
                }
            default:
                const pikachu: Pokemon = new PokemonBuilder()
                    .withNumber('25')
                    .withName('Pickachu')
                    .withDescription('Lorem Ipsum of pikachu')
                    .withHeight(1.3)
                    .withWeight(0.7)
                    .withAvatar('http://via.placeholder.com/475px475')
                    .build()
                const salameche: Pokemon = new PokemonBuilder()
                    .withNumber('4')
                    .withName('Salameche')
                    .withDescription('Lorem Ipsum of salameche')
                    .withHeight(1.7)
                    .withWeight(30)
                    .withAvatar('http://via.placeholder.com/475px475')
                    .build()
                const mewtwo: Pokemon = new PokemonBuilder()
                    .withNumber('150')
                    .withName('Mewtwo')
                    .withDescription('Lorem Ipsum of mewtwo')
                    .withHeight(2)
                    .withWeight(100)
                    .withAvatar('http://via.placeholder.com/475px475')
                    .build()

                return new InMemoryPokemonLoader([pikachu, salameche, mewtwo])
        }
    }
}
