import {Routes} from "@angular/router"
import {PokemonListingComponent} from "../adapters/primaries/pokemonlisting/pokemonListing.component"
import {PokemonDetailsComponent} from "../adapters/primaries/pokemondetails/pokemonDetails.component"

export const PokemonRoutes: Routes = [
    {path: 'pokemons', component: PokemonListingComponent},
    {path: 'pokemons/:number', component: PokemonDetailsComponent},
]