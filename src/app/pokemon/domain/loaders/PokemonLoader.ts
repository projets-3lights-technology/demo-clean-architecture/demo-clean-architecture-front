import {Observable} from 'rxjs/Observable'
import {Pokemon} from '../entities/pokemon'

export interface PokemonLoader {

    all(): Observable<Pokemon[]>

    get(number: String): Observable<Pokemon>

}