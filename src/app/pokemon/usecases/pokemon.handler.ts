import {Observable} from 'rxjs/Observable'
import {PokemonLoader} from '../domain/loaders/PokemonLoader'
import {Pokemon} from '../domain/entities/pokemon'

export class PokemonHandler {

    constructor(private pokemonSource: PokemonLoader) {

    }

    all(): Observable<Pokemon[]> {
        return this.pokemonSource.all()
    }

    get(number: String): Observable<Pokemon> {
        return this.pokemonSource.get(number)
    }
}