import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap'

import {AppComponent} from './app.component'
import {FormsModule} from '@angular/forms'
import {RouterModule} from '@angular/router'
import {PokemonModule} from './pokemon/adapters/primaries/pokemon.module'
import {AngularFireModule} from 'angularfire2'

const FirebaseConfig = {
    apiKey: process.env.FB_API_KEY,
    authDomain: process.env.FB_AUTH_DOMAIN,
    databaseURL: process.env.FB_DB_URL,
    projectId: process.env.FB_PROJECT_ID,
    storageBucket: process.env.FB_STORAGE_BUCKET,
    messagingSenderId: process.env.FB_MESSAGING_SENDER_ID
}

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        NgbModule.forRoot(),
        RouterModule.forRoot([{path: '', redirectTo: '/pokemons', pathMatch: 'full'}]),
        AngularFireModule.initializeApp(FirebaseConfig, process.env.FB_APP_NAME),
        PokemonModule
    ],
    declarations: [
        AppComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}